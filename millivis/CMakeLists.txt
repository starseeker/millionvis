# Minimum required version of CMake
cmake_minimum_required(VERSION 2.8)

# set CMake project name
project(MILLIONVIS)

# set bin and lib directories
if(NOT DEFINED BIN_DIR)
  set(BIN_DIR bin)
endif(NOT DEFINED BIN_DIR)
if(NOT DEFINED LIB_DIR)
  set(LIB_DIR lib)
endif(NOT DEFINED LIB_DIR)

find_package(Boost COMPONENTS filesystem REQUIRED)
find_package(Freetype)
find_package(OpenGL)
find_package(GLUT)
find_package(PNG)

add_definitions(-fpermissive)

include_directories(
  ${CMAKE_CURRENT_SOURCE_DIR}
  ${Boost_INCLUDE_DIRS}
  ${FREETYPE_INCLUDE_DIR_ft2build}
  ${OPENGL_INCLUDE_DIR}
  ${GLUT_INCLUDE_DIR}
  ${PNG_INCLUDE_DIR}
  )

add_subdirectory(boost)
add_subdirectory(infovis)
add_subdirectory(treemap2)
add_subdirectory(bench)
add_subdirectory(focusimage)

# Local Variables:
# tab-width: 8
# mode: cmake
# indent-tabs-mode: t
# End:
# ex: shiftwidth=2 tabstop=8
