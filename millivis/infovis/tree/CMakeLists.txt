set(libtree_srcs
	ObservableTree.cpp
	ObservableTree.hpp
	dir_property_tree.cpp
	dir_property_tree.hpp
	dir_tree.cpp
	dir_tree.hpp
	export_tree_xml.cpp
	export_tree_xml.hpp
	tree.cpp
	tree.hpp
	xml_property_tree.cpp
	xml_property_tree.hpp
	xml_tree.cpp
	xml_tree.hpp
	xmltree_tree.cpp
	xmltree_tree.hpp
	)

add_library(tree ${libtree_srcs})

add_executable(test_vector_as_tree test_vector_as_tree.cpp)
add_executable(test_sum_weight_visitor test_sum_weight_visitor.cpp)
add_executable(test_property_tree test_property_tree.cpp)
add_executable(test_copy test_copy.cpp)
add_executable(test_bfs_visitor test_bfs_visitor.cpp)
add_executable(test_tree test_tree.cpp)
add_executable(test_dir_tree test_dir_tree.cpp)
target_link_libraries(test_dir_tree tree)
add_executable(test_xml_tree test_xml_tree.cpp)
target_link_libraries(test_xml_tree tree)

